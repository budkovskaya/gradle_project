import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

public class GettingMonthNameTest {
    GettingMonthName gettingMonthName = new GettingMonthName();

    // Positive Test Cases
    @Test
    public void checkingCorrectDisplayingOfMonthName(){
        assertThat ("is not equals", gettingMonthName.returnMonthName(1), containsString("Jan"));
    }

    @Test
    public void checkMonthNameCorrespondsMonthNumber(){
        assertEquals("Feb", gettingMonthName.returnMonthName(2) );
    }

    @Test
    public void checkingValuesInMonthNotEmpty(){
        assertNotNull(gettingMonthName.returnMonthName(5));
    }

    //Negative Test Cases
    @Test
    public void checkingNegativeValuesForMonthNumber(){
        assertThat(gettingMonthName.returnMonthName(-1), containsString("cannot be negative value"));
    }

    @Test
    public void checkingValuesGreaterThanExistingMonthNumber(){
        assertTrue(gettingMonthName.returnMonthName(15).equals("There are only 12 month in a year. Number cannot be greater than 12."));
    }

    @Test
    public void checkingZeroValueInMonth(){
        assertFalse(gettingMonthName.returnMonthName(0).contains("negative value"));
    }
}
