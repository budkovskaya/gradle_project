import org.joda.time.DateTime;

public class GettingMonthName {
    // Returns month name depending on entered month number
    public String returnMonthName(int month) {
        if (month >= 1 && month <= 12) {
            String monthName = DateTime.now().withMonthOfYear(month).toString("MMM");
            return monthName;
        } else if (month < 0) {
            return "The number for month cannot be negative value";
        } else if (month > 12) {
            return "There are only 12 month in a year. Number cannot be greater than 12.";
        } else {
            return "There is no month with number 0";
        }
    }
}
